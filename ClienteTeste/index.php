<?php
$api = curl_init();
curl_setopt($api, CURLOPT_URL,"http://localhost:8000/api/tasks");
curl_setopt($api, CURLOPT_RETURNTRANSFER, true);
$api_response = json_decode(curl_exec($api));
curl_close($api);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <ul>
        <?php foreach($api_response as $task): ?>
            <li>
                <?= $task->name . $task->date; ?>
            </li>
        <?php endforeach; ?>
    </ul>    
</body>
</html>